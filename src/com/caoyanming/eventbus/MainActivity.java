package com.caoyanming.eventbus;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import com.caoyanming.netutil.NetUtil;

import android.app.Activity;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import de.greenrobot.event.EventBus;

/**
 * 
 * @author saymagic
 * 
 * blog:http://caoyanming.com
 *
 */
public class MainActivity extends Activity {


	TextView jokeTv = null;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		jokeTv = (TextView)findViewById(R.id.jokeTv);
		//注册EventBus事件
		EventBus.getDefault().register(this);
	}
	
	//根据参数类型来接收事件
	public void onEventMainThread(JSONObject jsonObject) {
		try {
			jokeTv.setText(jsonObject.get("text").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**
	这段代码不能与onEventMainThread同时出现，否则会报错：
	de.greenrobot.event.EventBusException: Illegal onEvent method, check for typos: public void so.cym.eventbus.MainActivity.onEventPostThread(org.json.JSONObject)
	
	public void onEventPostThread(JSONObject jsonObject) {
		Toast.makeText(this, "我收到事件会显示。", 2000).show();
	}
	*/
	/**
	 * 
	 * @param view
	 * 点击按钮后会向图灵机器人发起“讲笑话”的请求。
	 */
	public void tellJoke(View view){
		new Thread(){

			@Override
			public void run() {
				super.run();
				try {
					//发送EventBus事件
					EventBus.getDefault().post(new JSONObject(NetUtil.getJoke()));
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			};

		}.start();
	}

	@Override  
	public void onDestroy()  
	{  
		super.onDestroy();  
		// Unregister  
		EventBus.getDefault().unregister(this);  
	}  
}
